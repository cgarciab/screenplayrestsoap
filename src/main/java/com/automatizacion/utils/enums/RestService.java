package com.automatizacion.utils.enums;

public enum RestService {

    BASE_URL("https://reqres.in/api"),
    CREATE_USER("/users");

    private final String uri;

    RestService(String uri) {
        this.uri = uri;
    }

    @Override
    public String toString() {
        return this.uri;
    }

}
