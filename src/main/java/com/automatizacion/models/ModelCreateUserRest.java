package com.automatizacion.models;

public class ModelCreateUserRest {

    private final String name;
    private final String job;

    public ModelCreateUserRest(String name, String job) {
        this.name = name;
        this.job = job;
    }

    @Override
    public String toString() {
        return "{" +
                "\"name\":" + '\"' + this.name + '\"' +
                ",\"job\":" + '\"' + this.job + '\"' +
                '}';
    }
}
