package com.automatizacion.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class LastResponseStatusCode implements Question<Boolean> {

    private final int code;

    public LastResponseStatusCode(int code) {
        this.code = code;
    }

    public static LastResponseStatusCode isEqualsTo(int code) {
        return new LastResponseStatusCode(code);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        return lastResponse().statusCode() == this.code;
    }
}
