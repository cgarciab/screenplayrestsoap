package com.automatizacion.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Post;

import static com.automatizacion.utils.enums.RestService.CREATE_USER;

public class RestWithPost implements Interaction {

    private final String body;

    public RestWithPost(String body) {
        this.body = body;
    }

    public static RestWithPost withPost(String body) {
        return Tasks.instrumented(RestWithPost.class, body);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to(CREATE_USER.toString())
                        .with(
                                requestSpecification -> requestSpecification
                                        .header("Content-Type", "application/json")
                                        .body(body)
                        )
        );
    }


}
