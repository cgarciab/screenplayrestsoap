package com.automatizacion.taks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Post;

import java.util.List;

import static com.automatizacion.utils.enums.SoapService.ADD_NUMBERS;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class AddNumbersSoap implements Task {

    private final List<String> values;

    public AddNumbersSoap(List<String> values) {
        this.values = values;
    }

    public static AddNumbersSoap with(List<String> values) {
        return instrumented(AddNumbersSoap.class, values);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Post.to(ADD_NUMBERS.toString())
                        .with(request -> request
                                .header("Content-Type", "text/xml")
                                .body(
                                        "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                                                "<soap:Envelope\n" +
                                                " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                                                " xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"\n" +
                                                " xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                                                "	<soap:Body>\n" +
                                                "		<Add xmlns=\"http://tempuri.org/\">\n" +
                                                "			<intA>" + this.values.get(0) + "</intA>\n" +
                                                "			<intB>" + this.values.get(1) + "</intB>\n" +
                                                "		</Add>\n" +
                                                "	</soap:Body>\n" +
                                                " </soap:Envelope>"
                                )
                        )
        );
    }
}
