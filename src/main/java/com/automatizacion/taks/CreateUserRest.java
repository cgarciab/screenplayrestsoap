package com.automatizacion.taks;

import com.automatizacion.interactions.RestWithPost;
import com.automatizacion.models.ModelCreateUserRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

import java.util.List;

public class CreateUserRest implements Task {

    private final List<ModelCreateUserRest> modelCreateUserRest;

    public CreateUserRest(List<ModelCreateUserRest> modelCreateUserRest) {
        this.modelCreateUserRest = modelCreateUserRest;
    }

    public static CreateUserRest with(List<ModelCreateUserRest> modelCreateUserRest) {
        return Tasks.instrumented(CreateUserRest.class, modelCreateUserRest);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(RestWithPost.withPost(modelCreateUserRest.get(0).toString()));
    }
}
