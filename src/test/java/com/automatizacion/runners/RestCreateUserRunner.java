package com.automatizacion.runners;


import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/create_user.feature",
        glue = {"com.automatizacion.stepsdefinitions.rest", "com.automatizacion.utils.hooks.rest"},
        plugin = "pretty",
        snippets = SnippetType.CAMELCASE
)
public class RestCreateUserRunner {
}
