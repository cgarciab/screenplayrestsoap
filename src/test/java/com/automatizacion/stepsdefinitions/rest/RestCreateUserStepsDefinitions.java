package com.automatizacion.stepsdefinitions.rest;

import com.automatizacion.models.ModelCreateUserRest;
import com.automatizacion.questions.LastResponseStatusCode;
import com.automatizacion.taks.CreateUserRest;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;

import static com.automatizacion.utils.Constant.CODE_EXPECTED;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class RestCreateUserStepsDefinitions {

    @When("usted crea un usuario")
    public void ustedCreaUnUsuario(List<ModelCreateUserRest> modelCreateUserRest) {
        theActorInTheSpotlight().attemptsTo(CreateUserRest.with(modelCreateUserRest));
    }

    @Then("Verifico que el usario se ha creado")
    public void verificoQueElUsarioSeHaCreado() {
        theActorInTheSpotlight().should(
                seeThat(LastResponseStatusCode.isEqualsTo(CODE_EXPECTED))
        );
    }
}
