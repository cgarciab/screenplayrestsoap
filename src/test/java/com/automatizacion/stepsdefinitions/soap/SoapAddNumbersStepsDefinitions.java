package com.automatizacion.stepsdefinitions.soap;

import com.automatizacion.questions.LastResponseStatusCode;
import com.automatizacion.taks.AddNumbersSoap;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class SoapAddNumbersStepsDefinitions {

    @When("^Add two number$")
    public void addTwoNumber(List<String> values) {
        theActorInTheSpotlight().attemptsTo(AddNumbersSoap.with(values));
    }

    @Then("^Verify that the response of the service is (\\d+)$")
    public void verifyThatTheResponseOfTheServiceIs(int code) {
        theActorInTheSpotlight().should(
                seeThat("last response status code is 201", LastResponseStatusCode.isEqualsTo(code))
        );
    }
}
