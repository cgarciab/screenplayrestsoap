# Curso en Gradle de Rest y Soap en Screenplay 
El proyecto es un curso donde se realizo las siguientes automatizaciones:
 
- Automatización de una `RestApi`, donde se realiza una petición `Post`, en la api `https://reqres.in/api/users`.
- Automatización de una `SoapAPI`, donde se realiza una petición `Post`, en la api `http://www.dneonline.com/calculator.asmx`.

## Tecnologías Implementadas
- Lenguaje : Java 8_251
- IDE      : Intellij IDE 2020.1
- Management Tool : Gradle 6.1.1
- Framework Test : SerenityBDD 1.9.9
- Framework Test : Serenity-Rest 1.9.9
- Framework Test : Serenity-Cucumber 1.9.5
- Test-Driven : Junit 4.12

## Automatización Post a una RestApi
Se realiza una prueba automatiza RestApi, con la petición `POST` al Api User, donde se inserta un nuevo usuario con sus respectivos datos.

## Automatización página Web 
Se realiza una prueba automatiza SoapApi, con la petición `POST` al Api calculara, donde se agregan dos números y nos devuelve una suma.

## Ejecución por Terminal o Consola
La ejecución por consola trae ventajas al momento de probar.
- Nos permite ejecutar las pruebas sin necesidad de un IDE.
- También el tema de integración continua se hace más fácil su configuración.
- Entre muchas otras.

### Instrucciones 
- PASO 1: Ubicado dentro de la carpeta principal del proyecto `ScreenplayRestSoapAuto`, abrimos la terminal en esta ubicación.
- PASO 2: Escribimos el siguiente comando `gradle clean test aggregate`, luego ejecutamos.
- PASO 3: Esperamos que las pruebas se completen.
- PASO 4: Buscamos el index.html `target\site\serenity\index.html`, y lo abrimos con un navegador.
